import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms'
import { UserService } from '../user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

loginForm: FormGroup;
errorMessage: any;
submitted = false;

  constructor(private formBuilder: FormBuilder, private userService: UserService, private router: Router) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

onLoginSubmit(){
  this.submitted = true;

  if (this.loginForm.valid) {
    this.userService.login(this.loginForm.controls.email.value, this.loginForm.controls.password.value)
    .subscribe((response: any) => {
      if (response.message === 'userFound') {
        this.errorMessage = '';
        localStorage.setItem('userData', JSON.stringify(response));
        this.router.navigate(['/list']);
      } else {
        this.errorMessage = 'Invalid Credentials'
      }
    })
  }
}

}
