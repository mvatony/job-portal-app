import { Component, OnInit, ViewChild } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import { FormControl,FormGroup,FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import { UserService } from '../user.service';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})
export class ListUsersComponent implements OnInit {

  emptyflag: any;
  
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;


  constructor(private fb: FormBuilder, public  userService: UserService,public router:Router
    ) { }
    displayedColumns: string[] = ['SNO','name', 'email', 'mobileNumber', 'address', 'qualification','actions'];  dataSource: any;

  
  ngOnInit() {
    
    this.setDatatoGrid();
   
  }
  setDatatoGrid()
  {
    this.userService.getAllUsers().subscribe((data: any) => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;

      this.emptyflag =(this.dataSource && this.dataSource.filteredData.length > 0) ? false : true;
});
  }
  delete(id) {
  let x = confirm('Are you sure you want to delete the record');
  if (x) {
    this.userService.deleteUser(id._id).subscribe((data: any) => {
      this.setDatatoGrid();
   });
  }

  }

}