import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  baseUrl: string = 'http://localhost:9000/api/users';

  constructor(private http: HttpClient) { }

  getAllUsers() {
    return this.http.get(this.baseUrl);
  }
  login(email, password) {
    var url = `${this.baseUrl}/${email}/${password}`;
    return this.http.get(url);
  }

  createUser(user) {
    return this.http.post(this.baseUrl, user, {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    });
  }

  getUser(id) {
    var url = `${this.baseUrl}/${id}`;
    return this.http.get(url); 
  }

  updateUser(user, id) {
    var url = `${this.baseUrl}/${id}`;
    return this.http.put(url, user);
  }

  deleteUser(id) {
    var url = `${this.baseUrl}/${id}`;
    return this.http.delete(url);
  }

  isLoggedIn() {
    if(localStorage.getItem('userData')) {
      return true;
    } else {
      return false;
    }
  }

}
