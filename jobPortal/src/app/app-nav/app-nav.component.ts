import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-app-nav',
  templateUrl: './app-nav.component.html',
  styleUrls: ['./app-nav.component.css']
})
export class AppNavComponent implements OnInit {

  constructor( public userService: UserService, private router: Router) { }

  ngOnInit() {
    if (this.userService.isLoggedIn()) {
      this.router.navigate(['/list'])
    }
  }

logOut() {
  localStorage.removeItem('userData');
  this.router.navigate(['/login']);
}

}
