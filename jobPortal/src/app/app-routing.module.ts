import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateUserComponent } from './create-user/create-user.component';
import { LoginComponent } from './login/login.component';
import { ListUsersComponent } from './list-users/list-users.component';


const routes: Routes = [
  { path: 'create', component: CreateUserComponent },
  { path: 'login', component: LoginComponent },
  { path: 'list', component: ListUsersComponent },
  { path: 'create/:id', component: CreateUserComponent },
  { path: '', redirectTo: ',list', pathMatch: 'full' }  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
