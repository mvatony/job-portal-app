import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../user.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {

registerForm: FormGroup;
isEdit: boolean;
errorMessage: any;

  constructor(private formBuilder: FormBuilder, private userService: UserService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      mobileNumber: ['', Validators.required],
      address: ['', Validators.required],
      qualifications: ['', Validators.required]
    });

    if (this.route.snapshot.paramMap.get('id')) {
      this.isEdit = true;
      this.getUser(this.route.snapshot.paramMap.get('id'));
    }
  }

  getUser(id) {
    this.userService.getUser(id).subscribe((response: any) => {
      this.registerForm.controls.name.setValue(response.name);
      this.registerForm.controls.email.setValue(response.email);
      this.registerForm.controls.mobileNumber.setValue(response.mobileNumber);
      this.registerForm.controls.address.setValue(response.address);
      this.registerForm.controls.qualifications.setValue(response.qualifications);
      this.registerForm.controls.password.setValue(response.password);
    })
  }

  onSubmit() {
    if (this.registerForm.valid) {
      var requestObj = {
        name: this.registerForm.controls.name.value,
        email: this.registerForm.controls.email.value,
        mobileNumber: this.registerForm.controls.mobileNumber.value,
        password: this.registerForm.controls.password.value,
        address: this.registerForm.controls.address.value,
        qualifications: this.registerForm.controls.qualifications.value
      }

      if (this.isEdit) {
        var id = this.route.snapshot.paramMap.get('id');
        this.userService.updateUser(requestObj, id).subscribe((response: any) => {
          if (response === 'User Updated Succesfully') {
            this.router.navigate(['/list']);
          } else {
            this.errorMessage = response;
          }
        });
      } else {
        this.userService.createUser(requestObj).subscribe((response: any) => {
          if (response.message === 'user added succesfully') {
            this.router.navigate(['/login']);
          }else {
            this.errorMessage = response.message;
          }
        })
      }
    }
  }
}
