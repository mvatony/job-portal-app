var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
var mongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectId

var app = express();
app.use(cors());
app.use(bodyParser());

app.get('/api/users', function(req, resp){
    mongoClient.connect('mongodb://localhost:27017/jobPortal', function(err, db){
        if (!err) {
            db.collection('users').find().toArray(function(err, users){
                resp.send(JSON.stringify(users));
            });
        }
    });        
});

app.get('/api/users/:id', function(req, resp){
    mongoClient.connect('mongodb://localhost:27017/jobPortal', function(err, db){
        if (!err) {
            var idValue = new ObjectId(id);
            db.collection('users').findOne({_id: idValue}, function(err, user){
                resp.send(JSON.stringify(user));
            });
        }
    }); 
});

app.post('/api/users', function(req, resp){
    mongoClient.connect('mongodb://localhost:27017/jobPortal', function(err, db){
        if (!err) {
            var user = req.body; 
            db.collection('users').insertOne(user, function(err, result){
                if (!err) {
                    resp.send({'message': "user added successfully"});
                } else {
                    resp.send({'message': 'adding user failed'});
                }
            });
        }
    });
});

app.get('/api/users/:email/:password', function(req, resp){
    mongoClient.connect('mongodb://localhost:27017/jobPortal', function(err, db){
        if (!err) {
            var email = req.params.email;
            var password = req.params.password;
            db.collection('users').findOne({email: email, password: password}, function(err, response){
                if (!err && response != null) {
                    resp.send({'message': 'userFound'});
                } else {
                    resp.send({'message': 'userNotFound'});
                }
            });
        }
    });
});

app.put('/api/users/:id', function(req, resp){
    mongoClient.connect('mongodb://localhost:27017/jobPortal', function(err, db){
        if (!err) {
            var user = { $set: {name: req.body.name, email: req.body.email,
                         mobileNumber: req.body.mobileNumber, address: req.body.address, 
                         qualification: req.body.qualification, fileName: req.body.fileName} };
                         var idValue = new ObjectId(id);
            db.collection('users').updateOne({_id: idValue}, user, function(err, response){
                if (!err) {
                    resp.send('User Updated Successfully');
                } else {
                    resp.send('User Update failed');
                }
            });
        }
    });
});

app.delete('/api/users/:id', function(req, resp){
    mongoClient.connect('mongodb://localhost:27017/jobPortal', function(err, db){
        if (!err) {
            var idValue = new ObjectId(id);
            db.collection('users').remove({_id: idValue}, function(err, response){
                if (!err){
                    resp.send('User Removed Successfully');
                } else {
                    resp.send('User Removal Failure');
                }
            });
        }
    });
});

app.listen(9000, () => console.log('API started listening'));